# Installing and COnfiguring Traccar 4.10 with Mod

## Server Requisites:
* Ubuntu 18.04
* unzip

## Configuring Server
```
# apt update
# apt install unzip
```

### Changing Server Time
```
# sudo dpkg-reconfigure tzdata
```

### Creating SWAP
```
# sudo fallocate -l 2G /swapfile
# sudo chmod 600 /swapfile
# sudo mkswap /swapfile
# sudo swapon /swapfile
# sudo vi /etc/fstab
```
Add line:
```
/swapfile   none    swap    sw    0   0
```

## Installing Zabbix Agent
```
# sudo apt-get install zabbix-agent
# vim /etc/zabbix/zabbix_agentd.conf
```
Changing lines:
```
RemoteCommand= 1
Server=192.168.10.2
ServerActive=192.168.10.2
Hostname=Traccar_
```

```
# visudo
```
Add line:
```
zabbix ALL=NOPASSWD: ALL
```

## Installing Traccar
```
# cd /tmp
# wget https://github.com/traccar/traccar/releases/download/v4.10/traccar-linux-64-4.10.zip
# unzip traccar-linux-64-4.10.zip
# ./traccar.run
```

## Optmization Traccar
### Increasing connection limit on Linux
```
# vim /etc/security/limits.conf
```
Add lines:
```
* soft  nofile 50000
* hard  nofile 50000
```
```
# /etc/systemd/user.conf
```
Change lines:
```
DefaultLimitNOFILE=50000
```
```
# /etc/systemd/system.conf 
```
Change lines:
```
DefaultLimitNOFILE=50000
```

### Scaling beyond 65k connections
```
# vim /etc/sysctl.conf
```
Add Lines
```
vm.max_map_count = 250000
fs.file-max = 250000
net.ipv4.ip_local_port_range = 1024 65535
```

### Service configuration parameters
```
# vim /etc/systemd/system/traccar.service
```
Change Line:
```
ExecStart=/opt/traccar/jre/bin/java -Xms600m -Xmx600m -jar tracker-server.jar conf/traccar.xml
```

## Restart Server
```
# reboot
```